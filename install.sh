#!/bin/bash

set -xe

mkdir -p /usr/local/include/xolatile

cp xtandard.h /usr/local/include/xolatile/xtandard.h

exit

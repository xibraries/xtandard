# xtandard

## xtandard -- Header-only library for commonly used functions (in my style).

Install: `$ sudo sh install.sh`

Include: `#include <xolatile/xtandard.h>`

About this library:
- Disclaimer, my other projects depend on this header file, if you want to build them, install it.
- As someone who likes Ada, Fortran and Cowgol, some functions here resemble functions used in those languages.
- This library was written mainly for my own projects, but you can repurpose it for your own needs.
- Intended usage is writing simple programs, which can fit it one source file, often only in 'main' function.
- I'm not scared of using global variables, but there's not many of them, also rare "C-style" namespaces.

## Prototypes

Testing:

$$
\displaystyle\prod_{x=2}^4 x^2 = 2^2 \times 3^2 \times 4^2 = 576
$$
